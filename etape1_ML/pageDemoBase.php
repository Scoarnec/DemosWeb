<!doctype html>
<html>
    <!-- Métadonnées concernant la page -->
    <head>
        <!-- éléments obligatoires dans toute page HTML -->
        <title>Ma page de démo</title>
        <meta charset="utf-8">
    </head>
    <!-- Contenu de la page -->
    <body>
        <h1>Mes enseignements à l'ESIGELEC</h1>
        <h2>Présentation</h2>
        <p>Je suis enseignant dans le département TIC, responsable de la filière Big Data et référent école/ESRI.</p>
        <h2>Enseignements</h2>
        <h3>Cycle préparatoire</h3>
        <ol>
            <li>Algorithmique</li>
            <li>Langage C</li>
            <li>Bases de données</li>
        </ol>
        <h3>Tronc commun</h3>
        <ul>
            <li>JAVA</li>
        </ul>
        <h3>Parcours informatique</h3>
        <ul>
            <li>Conception de sites web</li>
        </ul>
        <h3>Dominantes</h3>
        <ol>
            <li>Analyses de données reproductible (BDTN)</li>
            <li>Large Scale Processing (BDTN)</li>
            <li>Projet Blockchain (IF)</li>
        </ol>
        <h3>Masters</h3>
        <ol>
            <li>Fundamentals of web development</li>
            <li>.NET framework</li>
        </ol>
        <h2>A propos de l'école</h2>
        <p>L’ESIGELEC forme des ingénieurs-es  généralistes dans le domaine des Systèmes Intelligents et Connectés, allant de l’Electronique automobile aéronautique, de l’Informatique, jusqu’à l’Energie, la Robotique ou encore au Biomédical, etc… Son programme est basé sur une pédagogie par projet, permettant la constitution d’un véritable projet personnel et professionnel. Il inclut une forte dimension internationale, innovation et entrepreneuriale.</p>
        <h2>Formulaire de contact</h2>
        <form method="POST" action="traitement.php">
            <section>
                <h4>Votre nom</h4>
                <p>
                    <label for="nom">Nom et prénom</label>
                    <input id="nom" name="nom" placeholder="Votre nom..." required>
                </p>      
            </section>
            <section>
                <h4>Comment vous répondre</h4>
                <fieldset>
                    <legend>
                        Vos coordonnées
                    </legend>
                    <p>
                        <label for="telnum">Numéro de téléphone</label>
                        <input type="tel" id="telnum" name="telnum" placeholder="+33123456789">
                    </p>
                    <p> 
                        <label for="courriel">Adresse de courriel</label>
                        <input type="email" id="courriel" name="courriel" placeholder="xxx@yyy.zz">
                    </p>
                </fieldset>
            </section>
            <section>
                <h4>Votre message</h4>
                <textarea placeholder="Votre message ici"></textarea>
            </section>
            <p> <button type="submit">Envoyer</button> </p>
        </form>
        <footer>
            <h2>Coordonnées</h2>
            <h3>Adresse postale</h3>
            <p>ESIGELEC : Technopôle du Madrillet<br>
                Avenue Galilée - BP 10024<br>
                76801 Saint-Etienne du Rouvray Cedex</p>
            <h3>Téléphone (standard)</h3>
            <p>(+33) 02 32 91 58 58</p>
            <h3>Email</h3>
            <p>tenier@esigelec.fr</p>
        </footer>
    </body>
</html>