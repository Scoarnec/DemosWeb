<!-- L'attribut lang="fr" identifie la page comme étant en français. D'après le w3c, identifier le langage de votre contenu vous permet d'effectuer plusieurs tâches automatiquement, du changement de l'apparence de votre page à l'extraction d'informations ou à la modification du fonctionnement d'une application. Certaines applications de langage fonctionnent sur l'ensemble du document, d'autres sur des parties du document convenablement étiquetées. -->
<!doctype html>
<html lang="fr">
    <!-- Métadonnées concernant la page -->
    <head>
        <!-- éléments obligatoires dans toute page HTML -->
        <title>Ma page de démo</title>
        <meta charset="utf-8">
        <!-- Chargement de la librairie font-awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Chargement de la feuille de style locale -->
        <link rel="stylesheet" href="styles.css">
    </head>
    <!-- Contenu de la page -->
    <body>
        <h1>Mes enseignements à l'ESIGELEC</h1>
        <!-- Intégration d'une ressource externe : une image hébergée sur le site de l'Esigelec -->
        <img id="imgEsigelec" src="http://www.esigelec.fr/sites/default/files/esigelec_logo.png" alt="logo Esigelec">
        <nav id="menu">
            <h4>Menu</h4>
            <ul>
                <li><a href="#presentation">Présentation</a></li>
                <li><a href="#enseignements">Enseignements</a></li>
                <li><a href="#lecole">L'école</a></li>
                <li><a href="#contactForm">Contact</a></li>
            </ul></nav>
        <h2 id="presentation">Présentation</h2>        <!-- si l'image est stockée en local, on indique le chemin d'accès : ici l'image est dans le répertoire img -->
        <p>Je suis enseignant dans le département TIC, responsable de la filière Big Data <img id="imgBigData" src="img/BigData.jpg" alt="bigdata"> et référent école/ESRI <img id="imgESRI" src="https://upload.wikimedia.org/wikipedia/en/6/6e/Esri_logo.svg" alt="logo ESRI"></p>
        <h2 id="enseignements">Enseignements</h2>
        <h3>Cycle préparatoire</h3>
        <header>
            <ol>
                <!-- Un lien dont la valeur de l'attribut href contient un # dirige vers la partie de la page identifiée par l'attribut id correspondant -->
                <li><a href="#algo">Algorithmique</a></li>
                <li><a href="#langageC">Langage C</a></li>
                <li><a href="#bdd">Bases de données</a></li>
            </ol>
        </header>
        <!-- L'attribut id identifie de manière unique un élément HTML dans la page. la valeur de l'attribut doit être unique dans la page -->
        <article id="algo">
            <h4>Algorithmique</h4>
            <p>L'algorithmique est l'étude et la production de règles et techniques qui sont impliquées dans la définition et la conception d'algorithmes, c'est-à-dire de processus systématiques de résolution d'un problème permettant de décrire précisément des étapes pour résoudre un problème algorithmique.</p>
            <!-- Un lien dont l'attribut href a pour valeur une URI absolue dirige vers une ressource d'un autre site -->
            <aside>Source : <a href="https://fr.wikipedia.org/wiki/Algorithmique">Wikipedia</a></aside>
        </article>
        <article id="langageC">
            <h4>Langage C</h4>
            <p>Le langage C a été mis au point par D.Ritchie et B.W.Kernighan au début des années 70. Leur but était de permettre de développer un langage qui permettrait d'obtenir un système d'exploitation de type UNIX portable</p>
            <aside>Source : <a href="http://www.commentcamarche.net/contents/113-langage-c">Comment ça marche</a></aside>
        </article>
        <article id="bdd">
            <h4>Bases de données</h4>
            <p>Un Système de Gestion de Base de Données (SGBD) est un logiciel qui permet de stocker des informations dans une base de données. Un tel système permet de lire, écrire, modifier, trier, transformer ou même imprimer les données qui sont contenus dans la base de données.</p>
            <aside>Source : <a href="http://sql.sh/sgbd">SQL.sh</a></aside>
        </article>
        <h3>Tronc commun</h3>
        <header>
            <ul>
                <li><a href="#java">Java</a></li>			
            </ul>
        </header>
        <article id="java">
            <h4>Java</h4>
            <p>Java est une technologie utilisée dans le développement d'applications pour rendre le Web à la fois plus divertissant et utile. Java et JavaScript sont bien distincts. JavaScript est une technologique simple permettant de créer des pages Web et exécutée uniquement dans votre navigateur.</p>
            <aside>Source: <a href="https://www.java.com/fr/about/whatis_java.jsp">java.com</a></aside>
        </article>
        <h3>Parcours informatique</h3>
        <header>
            <ul>
                <li><a href="#mosw">Conception de sites web</a></li>
            </ul>
        </header>
        <article id="mosw">
            <h4>Conception de sites web</h4>
            <p>Un site web, ou simplement site, est un ensemble de pages web et de ressources liées et accessible par une adresse web. Un site est hébergé sur un serveur web accessible via le réseau mondial internet ou un intranet local. L'ensemble des sites web constituent le World Wide Web.</p>
            <aside>Source : <a href="https://fr.wikipedia.org/wiki/Site_web">Wikipedia</a></aside>
        </article>
        <h3>Dominantes</h3>
        <header>
            <ol>
                <li><a href="#data">Analyses de données reproductible (BDTN)</a></li>
                <li><a href="#largescale">Large Scale Processing (BDTN)</a></li>
                <li><a href="#blockchain">Projet Blockchain (IF)</a></li>
            </ol>
        </header>
        <article id="data">
            <h4>Analyses de données reproductible (BDTN)</h4>
            <p>Comprendre et nettoyer des données réelles brutes issues de capteurs en utilisant le langage R. Produire un notebook permettant la reproduction de l’analyse. Projeter les informations dans un Système d'Information Géographique. Générer et publier une visualisation cartographique. Exploiter le ModelBuilder et l'API python d'ArcMap pour automatiser cette génération.</p>
            <!-- Lorsqu'un lien pointe vers une ressource autre qu'une page HTML, la bienséance veut que l'on indique le format du fichier et sa taille -->
            <aside>Source <a href="http://www.esigelec.fr/sites/default/files/form-doc/Livretpedagogique2015-2016.pdf">Livret pédagogique de l'ESIGELEC [PDF - 3,7Mo]</a></aside>
        </article>
        <article id="largescale">
            <h4>Large Scale Processing (BDTN)</h4>
            <p>Savoir expliquer l'écosystème hadoop/Spark. Mettre en place une architecture distribuée permettant d'effectuer des traitements batch et streaming en utilisant des conteneurs Docker. Déployer dans le cloud Azure</p>
            <aside>Source <a href="http://www.esigelec.fr/fr/formulaires/demander-une-documentation">Demande de documentation ESIGELEC</a></aside>
        </article>
        <article id="blockchain">
            <h4>Projet Blockchain (IF)</h4>
            <p>Ecrire un Smart Contract Ethereum avec Solidity. Concevoir et déployer une application décentralisée s'appuyant sur Ethereum.</p>
            <aside>Source <a href="http://www.esigelec.fr/fr/formulaires/demander-une-documentation">Demande de documentation ESIGELEC</a></aside>
        </article>
        <h3>Masters</h3>
        <ol>
            <li><a href="#webdev">Fundamentals of web development</a></li>
            <li><a href="#dotnet">.NET framework</a></li>
        </ol>
        <!-- L'attribut lang permet de définir un langage différent pour l'élément et ses descendants -->
        <article id="webdev" lang="en">
            <h4>Fundamentals of web development</h4>
            <p>Web development is a broad term for the work involved in developing a web site for the Internet (World Wide Web) or an intranet (a private network). Web development can range from developing the simplest static single page of plain text to the most complex web-based internet applications (or just 'web apps') electronic businesses, and social network services. A more comprehensive list of tasks to which web development commonly refers, may include web engineering, web design, web content development, client liaison, client-side/server-side scripting, web server and network security configuration, and e-commerce development.</p>
            <aside>Source : <a href="https://en.wikipedia.org/wiki/Web_development">Wikipedia [en]</a></aside>
        </article>
        <article id="dotnet" lang="en">
            <h4>.NET framework</h4>
            <p>.NET is a general purpose development platform. It has several key features, such as support for multiple programming languages, asynchronous and concurrent programming models, and native interoperability, which enable a wide range of scenarios across multiple platforms.</p>
            <aside>Source : <a href="https://docs.microsoft.com/en-us/dotnet/standard/tour">Microsoft docs [en]</a></aside>
        </article>
        <h2 id="lecole">A propos de l'école</h2>
        <p>L’ESIGELEC forme des ingénieurs-es  généralistes dans le domaine des Systèmes Intelligents et Connectés, allant de l’Electronique automobile aéronautique, de l’Informatique, jusqu’à l’Energie, la Robotique ou encore au Biomédical, etc… Son programme est basé sur une pédagogie par projet, permettant la constitution d’un véritable projet personnel et professionnel. Il inclut une forte dimension internationale, innovation et entrepreneuriale.</p>
        <!-- <iframe>  permet d'intégrer le contenu d'un autre document dans la page courante -->
        <iframe id="videoEsigelec" src="https://www.youtube.com/embed/4kHLn5MMjtk"></iframe>
        <h2 id="contactForm">Formulaire de contact</h2>
        <form method="POST" action="traitement.php">
            <section>
                <h4 class="formtitle">Votre nom</h4>
                <p>
                    <label for="nom">Nom et prénom</label>
                    <input id="nom" name="nom" placeholder="Votre nom..." required>
                </p>      
            </section>
            <section>
                <h4 class="formtitle">Comment vous répondre</h4>
                <fieldset>
                    <legend>
                        Vos coordonnées
                    </legend>
                    <p>
                        <label for="telnum">Numéro de téléphone</label>
                        <input type="tel" id="telnum" name="telnum" placeholder="+33123456789">
                    </p>
                    <p> 
                        <label for="courriel">Adresse de courriel</label>
                        <input type="email" id="courriel" name="courriel" placeholder="xxx@yyy.zz">
                    </p>
                </fieldset>
            </section>
            <section>
                <h4 class="formtitle">Votre message</h4>
                <textarea placeholder="Votre message ici"></textarea>
            </section>
            <p> <button type="submit">Envoyer</button> </p>
        </form>
        <footer id="basdepage">
            <address>
                <div id="adresse">
                    <p>ESIGELEC : Technopôle du Madrillet<br>
                        Avenue Galilée - BP 10024<br>
                        76801 Saint-Etienne du Rouvray Cedex</p>
                </div>
                <div id="contact">
                    <p><i class="fa fa-phone fa-lg"></i>&nbsp;(+33) 02 32 91 58 58</p>
                    <p><i class="fa fa-envelope fa-lg"></i>&nbsp;tenier@esigelec.fr</p>
                </div>
            </address>
        </footer>
    </body>
</html>