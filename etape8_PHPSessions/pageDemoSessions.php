<?php require_once("conf_accueil.inc.php");
include 'header.inc.php'; ?>
<!-- Contenu de la page -->
<body>
    <!-- Une page boostrap débute généralement par la barre de menu. La classe fixed-top permet la fixer en haut -->
    <?php include 'menu.inc.php'; ?>
    <!-- La page d'accueil contient souvent un Jumbotron, qui véhicule le message fort de la page -->
    <div class="jumbotron">
        <h1 class="display-3">Mes enseignements à l'ESIGELEC</h1>
        <p class="lead">Je suis enseignant dans le département TIC, responsable de la filière Big Data <img id="imgBigData" src="img/BigData.jpg" alt="bigdata"> et référent école/ESRI <img id="imgESRI" src="https://upload.wikimedia.org/wikipedia/en/6/6e/Esri_logo.svg" alt="logo ESRI">.</p>
    </div>
    <div class="container">
        <div class="card" style="width: 20rem;">
            <div class="card-header">
                <img class="card-img-top" src="http://www.imghans.com/wp-content/uploads/2015/05/Welcome-In-Green-Background.png" alt="Welcome">
            </div>
            <div class="card-body">
                <h4 class="card-title">Bienvenue</h4>
                <p class="card-text"><?php echo "Nous sommes le  " . (new \DateTime())->format('d/m/Y') . "<br>Il est " .(new \DateTime())->format('H:i:s') ; ?> </p>
                <p class="card-text">
                    <?php
                    if(isset($_SESSION["dejavu"])){
                        echo 'Ravi de vous revoir !';
                    }else{
                        echo 'C\'est votre première visite ?';
                        $_SESSION["dejavu"]="ok";
                    }
                    ?>
                </p>
                <?php
                if(isset($_SESSION["dejavu"])){
                    echo '<a href="#" id="oubli" class="btn btn-primary">Oubliez-moi !</a>';
                }
                ?>
            </div>
        </div>
    </div>
    <h2 id="enseignements">Enseignements</h2>
    <!-- Les enseignements vont être placés dans un conteneur pour travailler sur une grille fluide : selon la taille de l'écran, les éléments occuperont une ou plusieurs colonnes -->
    <div class="container">
        <!-- sur un téléphone le contenu reste sur une colonne. Sur grand écran, 3 colonnes sont utilisées -->
        <div class="row">
            <div class="col-12">
                <h3 class="bg-info text-light">Cycle préparatoire</h3>
                <header>
                    <ol>
                        <!-- Un lien dont la valeur de l'attribut href contient un # dirige vers la partie de la page identifiée par l'attribut id correspondant -->
                        <li><a href="#algo">Algorithmique</a></li>
                        <li><a href="#langageC">Langage C</a></li>
                        <li><a href="#bdd">Bases de données</a></li>
                    </ol>
                </header>
            </div>
            <!-- exemple adaptatif : 1 colonne sur smartphone, 2 sur écran moyen, 3 sur grand écran -->
            <div class="col-12 col-md-6 col-lg-4">
                <!-- L'attribut id identifie de manière unique un élément HTML dans la page. la valeur de l'attribut doit être unique dans la page -->
                <article id="algo">
                    <h4>Algorithmique</h4>
                    <p>L'algorithmique est l'étude et la production de règles et techniques qui sont impliquées dans la définition et la conception d'algorithmes, c'est-à-dire de processus systématiques de résolution d'un problème permettant de décrire précisément des étapes pour résoudre un problème algorithmique.</p>
                    <!-- Un lien dont l'attribut href a pour valeur une URI absolue dirige vers une ressource d'un autre site -->
                    <aside>Source : <a href="https://fr.wikipedia.org/wiki/Algorithmique">Wikipedia</a></aside>
                </article>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <article id="langageC">
                    <h4>Langage C</h4>
                    <p>Le langage C a été mis au point par D.Ritchie et B.W.Kernighan au début des années 70. Leur but était de permettre de développer un langage qui permettrait d'obtenir un système d'exploitation de type UNIX portable</p>
                    <aside>Source : <a href="http://www.commentcamarche.net/contents/113-langage-c">Comment ça marche</a></aside>
                </article>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <article id="bdd">
                    <h4>Bases de données</h4>
                    <p>Un Système de Gestion de Base de Données (SGBD) est un logiciel qui permet de stocker des informations dans une base de données. Un tel système permet de lire, écrire, modifier, trier, transformer ou même imprimer les données qui sont contenus dans la base de données.</p>
                    <aside>Source : <a href="http://sql.sh/sgbd">SQL.sh</a></aside>
                </article>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3 class="bg-info text-light mt-5">Tronc commun</h3>
                <header>
                    <ul>
                        <li><a href="#java">Java</a></li>
                    </ul>
                </header>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <article id="java">
                    <h4>Java</h4>
                    <p>Java est une technologie utilisée dans le développement d'applications pour rendre le Web à la fois plus divertissant et utile. Java et JavaScript sont bien distincts. JavaScript est une technologique simple permettant de créer des pages Web et exécutée uniquement dans votre navigateur.</p>
                    <aside>Source: <a href="https://www.java.com/fr/about/whatis_java.jsp">java.com</a></aside>
                </article>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3 class="bg-info text-light mt-5">Parcours informatique</h3>
                <header>
                    <ul>
                        <li><a href="#mosw">Conception de sites web</a></li>
                    </ul>
                </header>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <article id="mosw">
                    <h4>Conception de sites web</h4>
                    <p>Un site web, ou simplement site, est un ensemble de pages web et de ressources liées et accessible par une adresse web. Un site est hébergé sur un serveur web accessible via le réseau mondial internet ou un intranet local. L'ensemble des sites web constituent le World Wide Web.</p>
                    <aside>Source : <a href="https://fr.wikipedia.org/wiki/Site_web">Wikipedia</a></aside>
                </article>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3 class="bg-info text-light mt-5">Dominantes</h3>
                <!-- Exemple d'accordeon - attention nécessite JavaScript activé pour fonctionner -->
                <noscript><p class="alert alert-danger">JavaScript doit être activé pour accéder au contenu de l'accordéon</p></noscript>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10" id="accDominantes" role="tablist">
                <div class="card">
                    <div class="card-header" role="tab" id="headerData">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#data" aria-expanded="true" aria-controls="data">Analyses de données reproductible (BDTN)</a>
                        </h5>
                    </div>
                    <div id="data" class="collapse show" role="tabpanel" aria-labelledby="headerData" data-parent="#accDominantes">
                        <p>Comprendre et nettoyer des données réelles brutes issues de capteurs en utilisant le langage R. Produire un notebook permettant la reproduction de l’analyse. Projeter les informations dans un Système d'Information Géographique. Générer et publier une visualisation cartographique. Exploiter le ModelBuilder et l'API python d'ArcMap pour automatiser cette génération.</p>
                        <!-- Lorsqu'un lien pointe vers une ressource autre qu'une page HTML, la bienséance veut que l'on indique le format du fichier et sa taille -->
                        <aside>Source : <a href="http://www.esigelec.fr/sites/default/files/form-doc/Livretpedagogique2015-2016.pdf">Livret pédagogique de l'ESIGELEC [PDF - 3,7Mo]</a></aside>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" role="tab" id="headerLargeScale">
                        <h5 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#largescale" aria-expanded="false" aria-controls="largescale">Large Scale Processing (BDTN)</a>
                        </h5>
                    </div>
                    <div id="largescale" class="collapse" role="tabpanel" aria-labelledby="headerLargeScale" data-parent="#accDominantes">
                        <p>Savoir expliquer l'écosystème hadoop/Spark. Mettre en place une architecture distribuée permettant d'effectuer des traitements batch et streaming en utilisant des conteneurs Docker. Déployer dans le cloud Azure</p>
                        <aside>Source <a href="http://www.esigelec.fr/fr/formulaires/demander-une-documentation">Demande de documentation ESIGELEC</a></aside>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" role="tab" id="headerBlockchain">
                        <h5 class="mb-0">
                            <a class="collapsed" data-toggle="collapse" href="#blockchain" aria-expanded="false" aria-controls="largescale">Projet Blockchain (IF)</a>
                        </h5>
                    </div>
                    <div id="blockchain" class="collapse" role="tabpanel" aria-labelledby="headerBlockchain" data-parent="#accDominantes">
                        <p>Ecrire un Smart Contract Ethereum avec Solidity. Concevoir et déployer une application décentralisée s'appuyant sur Ethereum.</p>
                        <aside>Source <a href="http://www.esigelec.fr/fr/formulaires/demander-une-documentation">Demande de documentation ESIGELEC</a></aside>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3 class="bg-info text-light mt-5">Masters</h3>
                <ol>
                    <li><a href="#webdev">Fundamentals of web development</a></li>
                    <li><a href="#dotnet">.NET framework</a></li>
                </ol>
            </div>
            <!-- L'attribut lang permet de définir un langage différent pour l'élément et ses descendants -->
            <article id="webdev" lang="en" class="col-12 col-md-6 col-lg-4">
                <h4>Fundamentals of web development</h4>
                <p>Web development is a broad term for the work involved in developing a web site for the Internet (World Wide Web) or an intranet (a private network). Web development can range from developing the simplest static single page of plain text to the most complex web-based internet applications (or just 'web apps') electronic businesses, and social network services. A more comprehensive list of tasks to which web development commonly refers, may include web engineering, web design, web content development, client liaison, client-side/server-side scripting, web server and network security configuration, and e-commerce development.</p>
                <aside>Source : <a href="https://en.wikipedia.org/wiki/Web_development">Wikipedia [en]</a></aside>
            </article>
            <article id="dotnet" lang="en" class="col-12 col-md-6 col-lg-4">
                <h4>.NET framework</h4>
                <p>.NET is a general purpose development platform. It has several key features, such as support for multiple programming languages, asynchronous and concurrent programming models, and native interoperability, which enable a wide range of scenarios across multiple platforms.</p>
                <aside>Source : <a href="https://docs.microsoft.com/en-us/dotnet/standard/tour">Microsoft docs [en]</a></aside>
            </article>
        </div>
    </div>


    <h2 id="lecole">A propos de l'école</h2>
    <div class="container">
        <!-- sur un téléphone le contenu reste sur une colonne. Sur grand écran, 3 colonnes sont utilisées -->
        <div class="row">
            <div class="col-12 col-lg-6">
                <p>L’ESIGELEC forme des ingénieurs-es  généralistes dans le domaine des Systèmes Intelligents et Connectés, allant de l’Electronique automobile aéronautique, de l’Informatique, jusqu’à l’Energie, la Robotique ou encore au Biomédical, etc… Son programme est basé sur une pédagogie par projet, permettant la constitution d’un véritable projet personnel et professionnel. Il inclut une forte dimension internationale, innovation et entrepreneuriale.</p>
            </div>
            <div class="col-12 col-lg-5 ml-lg-auto">
                <!-- <iframe>  permet d'intégrer le contenu d'un autre document dans la page courante -->
                <iframe id="videoEsigelec" src="https://www.youtube.com/embed/4kHLn5MMjtk"></iframe>
            </div>
        </div>
    </div>

    <h2 id="contactForm">Formulaire de contact</h2>
    <div class="container ">
        <form method="POST" action="traitement.php">
            <section>
                <h4 class="formtitle">Votre nom</h4>
                <div class="form-group form-row">
                    <label class="col-sm-4" for="nom">Nom et prénom</label>
                    <input class="form-control col-sm-8" id="nom" name="nom" placeholder="Votre nom..." required>
                    <div class="invalid-feedback">
                        Please provide a valid city.
                    </div>
                </div>
            </section>
            <section>
                <h4 class="formtitle">Comment vous répondre</h4>
                <fieldset>
                    <legend>
                        Vos coordonnées
                    </legend>
                    <div class="form-group form-row">
                        <label class="col-sm-4" for="telnum">Numéro de téléphone</label>
                        <input class="form-control col-sm-8" type="tel" id="telnum" name="telnum" placeholder="+33123456789">
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <label class="col-sm-4" for="courriel">Adresse de courriel</label>
                            <input class="form-control col-sm-8" type="email" id="courriel" name="courriel" placeholder="xxx@yyy.zz">
                        </div>
                        <div class="form-row">
                            <div class="col-sm-4"></div>
                            <small id="courrielHelp" class="form-text text-muted col-sm-8">Votre email ne sera pas vendu !</small>
                        </div>
                    </div>
                </fieldset>
            </section>
            <section>
                <h4 class="formtitle">Votre message</h4>
                <div class="form-row">
                    <div class="col-sm-4"></div>
                    <textarea minlength="20"  rows="5" class="form-control col-sm-8" placeholder="Votre message ici"></textarea>
                </div>
                <div class="form-row">
                    <div class="col-sm-4"></div>
                    <small id="textlHelp" class="form-text text-muted col-sm-8">Veuillez saisir au moins 20 caractères</small>
                </div>
            </section>
            <p> <button class="btn btn-primary" type="submit">Envoyer</button> </p>
        </form>
    </div>
    <footer id="basdepage" class="bg-secondary mt-3 pt-2">
        <div class="container">
            <div class="row">
                <div id="adresse" class="col-12 col-md-6">
                    <address>
                        <p>ESIGELEC : Technopôle du Madrillet<br>
                            Avenue Galilée - BP 10024<br>
                            76801 Saint-Etienne du Rouvray Cedex</p>
                    </address>
                </div>
                <div id="contact" class="col-12 col-md-6">
                    <p><i class="fa fa-phone fa-lg"></i>&nbsp;(+33) 02 32 91 58 58</p>
                    <p><i class="fa fa-envelope fa-lg"></i>&nbsp;tenier@esigelec.fr</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Ajout des scripts pour Bootstrap
jQuery, puis Popper.js, et enfin Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>
